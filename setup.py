import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name='qasearch',
    version='0.0.1a1',
    author='Andrey Poletaev',
    author_email='poletaev.andrew@gmail.com',
    description='Search models for text embeddings',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/karpov-hardml-course-final-project/repository-6-knn-indices',
    project_urls = {
        "Bug Tracker": "https://gitlab.com/karpov-hardml-course-final-project/repository-6-knn-indices/issues"
    },
    packages=['qasearch'],
    install_requires=['numpy==1.23.4', 'faiss-cpu==1.7.3'],
)