from collections import defaultdict

import os
import pickle
import json
from typing import List

import numpy as np
import faiss


class SearchModel:
    def __init__(self, embeddings: dict, docs: List[str]) -> None:
        emb = [embeddings[t].reshape(-1) for t in docs]
        self.docs = docs
        self.embeddings = np.array(emb)
        self.idxs = np.arange(len(docs))
        self.dim = self.embeddings.shape[1]
        # build indices
        self.findex = faiss.IndexFlatIP(self.dim)
        self.findex = faiss.IndexIDMap(self.findex)
        self.findex.add_with_ids(self.embeddings, self.idxs)
        
    def search(self, query: np.array, k: int = 10) -> List[str]:
        q = query.reshape(1, -1)
        if q.shape[1] != self.dim:
            raise ValueError(f"incorrect query shape {q.shape}!")
        # search index
        _, j = self.findex.search(q, k=k)
        return [self.docs[ind] for ind in j[0]]



class MetaSearchModel:
    def __init__(self) -> None:
        self.models = defaultdict(SearchModel)
    
    def add(self, key: str, model: SearchModel) -> None: 
        self.models[key] = model

    def search(self, cluster: str, query: list, k:int=10):
        q = np.array(query, dtype=np.float32)
        return self.models[cluster].search(query=q, k=k)

    @classmethod
    def deserialize(cls, path):
        with open(path, 'rb') as f:
            model = pickle.load(f)      
        return model  



if __name__ == "__main__":

    for gen in [1, 2]:
        # load data from files
        data_path = os.environ.get("DATA_DIR")
        print(data_path)

        # load embeddings
        with open(os.path.join(data_path, f"use_embeddings_dg{gen}.pkl"), 'rb') as f:
            emb1 = pickle.load(f)

        # load clusters json
        with open(os.path.join(data_path, f"clusters_use_dg{gen}.json"), 'rt') as f:
            clust1 = json.load(f)

        emb = [emb1[t].reshape(-1) for t in clust1['0']]

        # add indices into meta model
        meta = MetaSearchModel()
        for k in clust1.keys():
            print(k)
            meta.add(k, SearchModel(emb1, clust1[k]))
        
        # save pickle
        with open(os.path.join(data_path, f"index{gen}.pkl"), 'wb') as f:
            pickle.dump(meta, f, pickle.HIGHEST_PROTOCOL)


        # load pickle
        with open(os.path.join(data_path, f"index{gen}.pkl"), 'rb') as f:
            # The protocol version used is detected automatically, so we do not
            # have to specify it.
            model2 = pickle.load(f)

        # check if are equal
        assert(meta.search('0', emb[0], 3) == model2.search('0', emb[0], 3))

        # another way
        model3 = MetaSearchModel.deserialize(os.path.join(data_path, f"index{gen}.pkl"))
        assert(meta.search('0', emb[0], 3) == model3.search('0', emb[0], 3))
